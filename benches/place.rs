#![feature(test)]

extern crate test;
extern crate jenga;

use test::*;
use jenga::*;

#[inline]
fn fib(data: &mut [usize], n: usize) -> usize {
    data[0] = 1;
    data[1] = 1;
    for i in 2 .. n {
        data[i] = data[i-1] + data[i-2];
    }
    data[n]
}

#[bench]
fn test_fixed_fib_10(b: &mut Bencher) {
    b.iter(|| {
        let mut data = [0usize; 11];
        let f = fib(&mut data, 10);
        black_box(f);
    })
}

#[bench]
fn test_try_place_fib_10(b: &mut Bencher) {
    init_with_capacity(1000);
    frame(|f| {
        b.iter(|| {
            let mut sub = f.subframe();
            let mut data = sub.try_place(11).unwrap();
            let f = fib(&mut data, 10);
            black_box(f);
        })
    })
}
#[bench]
fn test_place_fib_10(b: &mut Bencher) {
    init_with_capacity(1000);
    frame(|f| {
        b.iter(|| {
            let mut sub = f.subframe();
            let mut data = sub.place(11);
            let f = fib(&mut data, 10);
            black_box(f);
        })
    })
}
#[bench]
fn test_fixed_fib_100(b: &mut Bencher) {
    b.iter(|| {
        let mut data = [0usize; 101];
        let f = fib(&mut data, 100);
        black_box(f);
    })
}

#[bench]
fn test_jenga_fib_100(b: &mut Bencher) {
    init_with_capacity(1000);
    frame(|f| b.iter(|| {
        let mut s = f.subframe();
        let mut data = s.place(101);
        let f = fib(&mut data, 100);
        black_box(data);
    }));
}
