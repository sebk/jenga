#![feature(test)]

extern crate test;
extern crate jenga;

use test::*;
use jenga::*;
use std::fmt::Write;

#[bench]
fn test_1000_bottles_jenga(b: &mut Bencher) {
    init_with_capacity(1 << 23);
    frame(|f| b.iter(|| {
        let mut s = f.subframe();
        let mut w = s.writer();
        for i in (0 .. 1000).rev() {
            write!(w, "\
                {} bottles of beer on the wall, \
                {} bottles of beer. \
                Take one down and pass it around, \
                {} bottle of beer on the wall. \
                ",
                i + 1, i + 1, i
            ).unwrap();
        }
        black_box(w.finish());
    }));
}

#[bench]
fn test_1000_bottles_string(b: &mut Bencher) {
    b.iter(|| {
        let mut s = String::new();
        for i in (0 .. 1000).rev() {
            write!(s, "\
                {} bottles of beer on the wall, \
                {} bottles of beer. \
                Take one down and pass it around, \
                {} bottle of beer on the wall. \
                ",
                i + 1, i + 1, i
            ).unwrap();
        }
        black_box(s);
    })
}

#[bench]
fn test_hello42_jenga(b: &mut Bencher) {
    frame(|f| b.iter(|| {
        let mut s = f.subframe();
        let mut w = s.writer();
        write!(w, "Hello {}", 42).unwrap();
        black_box(w.finish());
    }));
}
#[bench]
fn test_hello42_jenga_no_frame(b: &mut Bencher) {
    b.iter(|| frame(|f| {
        let mut w = f.writer();
        write!(w, "Hello {}", 42).unwrap();
        black_box(w.finish());
    }));
}

#[bench]
fn test_hello42_string(b: &mut Bencher) {
    b.iter(|| black_box(format!("Hello {}", 42)))
}
