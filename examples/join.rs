extern crate jenga;
use jenga::*;
use std::fmt::Write;

fn join_str<'a>(frame: &mut Frame<'a>, a: &str, b: &str) -> &'a str {
    let mut w = frame.writer();
    w.write_str(a).unwrap();
    w.write_str(b).unwrap();
    w.finish()
}

fn main() {
    let mut data = [0; 1024];
    let mut frame = Frame::from_slice(&mut data);
    println!("{}", join_str(&mut frame, "Hello ", "World!\n"));
    
    let r = frame.place_iter(::std::iter::repeat(42usize));
    println!("inf: {:?}", r);
}
