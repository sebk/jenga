#![feature(test)]

extern crate test;
extern crate jenga;

use test::*;
use jenga::*;


pub type N = usize;

pub struct Collatz {
    n:  N
}
impl Collatz {
    pub fn from(n: N) -> Collatz {
        Collatz { n: n }
    }
}
impl Iterator for Collatz {
    type Item = N;
    #[inline]
    fn next(&mut self) -> Option<N> {
        if self.n == 1 {
            None
        } else if self.n % 2 == 0 {
            self.n = self.n / 2;
            Some(self.n)
        } else {
            self.n = self.n * 3 + 1;
            Some(self.n)
        }
    }
}

#[bench]
fn test_vec_collatz_1000(b: &mut Bencher) {
    b.iter(|| {
        let v: Vec<_> = Collatz::from(1000).collect();
        black_box(v);
    })
}

#[bench]
fn test_place_iter_collatz_1000(b: &mut Bencher) {
    b.iter(|| {
        place_iter(Collatz::from(1000), |data| {
            black_box(data);
        })
    })
}
#[bench]
fn test_vec_collatz_10(b: &mut Bencher) {
    b.iter(|| {
        let v: Vec<_> = Collatz::from(10).collect();
        black_box(v);
    })
}

#[bench]
fn test_place_iter_collatz_10(b: &mut Bencher) {
    b.iter(|| {
        place_iter(Collatz::from(10), |data| {
            black_box(data);
        })
    })
}
