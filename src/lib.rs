#![feature(thread_local)]
#![feature(const_fn)]
#![cfg_attr(not(feature = "std"), no_std)]
#![cfg_attr(feature = "alloc", feature(alloc, allocator_api))]


/*!
Jenga: A stack based allocator.

## General principles: 
A Frame is created from a mutable buffer. This Buffer will be used for all allocations in this frame.

```rust
let mut buffer = vec![0u8; 1000_000];
let mut frame = jenga::Frame::from_slice(&mut buffer);
let mut vec = frame.vec(100);
vec.push(42);
vec.push(3);
```
*/

#[cfg(feature = "std")]
extern crate core;
#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "std")]
use std::io;

use core::{mem, slice, ptr, ops, marker, fmt, str};
use ops::{Deref, DerefMut};
use marker::{PhantomData};
use core::ptr::NonNull;

#[derive(Debug)]
pub struct InitError {}

#[derive(Debug)]
pub struct OutOfMemory {}

/**
An allocation Frame.

Data can be allocated using:

 - `place` which allocates b bytes of type T and returns a mutable view to it.
 - `place_raw` if you really want unsafe code.
 - `place_iter` will drain the given iterator and return a mutable view to the data.
 - `cursor` gives you a gowing buffer that supports `io::Write`.
 - `writer` does the same, but supports `fmt::Write` and is can be turned into `&str`
 
All returned buffers are valid for the lifetime of the `Frame`.
*/

pub struct Frame<'a> {
    top:    usize,
    cap:    usize,
    _m:     PhantomData<&'a ()>
}

#[inline(always)]
fn padding_for<T>(ptr: usize) -> usize {
    let align = mem::align_of::<T>();
    align - (ptr - 1) % align - 1
}

impl<'a> Frame<'a> {
    /// Turn a slice of memory into an allocator Frame.
    pub fn from_slice(s: &'a mut [u8]) -> Frame<'a> {
        let ptr = s.as_mut_ptr() as usize;
        let len = s.len();
        
        Frame {
            top:    ptr,
            cap:    ptr + len,
            _m:     PhantomData
        }
    }
    
    /// Allocate a vector that can hold up to`capacity` elements of `T`.
    /// Returns `OutOfMemory` if there is not enough space left.
    #[inline]
    pub fn try_place<T>(&mut self, capacity: usize) -> Result<Vec<'a, T>, OutOfMemory>
    {
        unsafe {
            let ptr = self.try_place_raw(capacity)?;
            Ok(Vec::from_raw(ptr.as_ptr(), capacity))
        }
    }
    
    /// Allocate a vector that can hold up to`capacity` elements of `T`.
    /// panics if there is not enough space left.
    #[inline]
    pub fn vec<T>(&mut self, capacity: usize) -> Vec<'a, T>
    {
        unsafe {
            let ptr = self.place_raw(capacity);
            Vec::from_raw(ptr, capacity)
        }
    }
    
    #[inline]
    pub fn put<T>(&mut self, t: T) -> FBox<'a, T> {
        unsafe {
            let raw_ptr = self.place_raw(1);
            ptr::write(raw_ptr, t);
            FBox {
                ptr: raw_ptr,
                _m: PhantomData
            }
        }
    }
    
    /// Allocate `count` elements of `T`.
    /// Returns `OutOfMemory` if there is not enough space left.
    /// The caller has to ensure the data does not outlife the `Frame`
    #[inline(always)]
        pub unsafe fn try_place_raw<T>(&mut self, count: usize)
                                       -> Result<NonNull<T>, OutOfMemory>
    {
        let size = count * mem::size_of::<T>();
        let pad = padding_for::<T>(self.top);
        let ptr = self.top + pad;
        let new_top = ptr + size;
        
        if new_top < self.cap {
            // update top pointer
            self.top = new_top;
            Ok(NonNull::new_unchecked(ptr as *mut T))
        } else {
            #[cold]
            Err(OutOfMemory{})
        }
    }
    
    /// Allocate `count` elements of `T`.
    /// panics if there is not enough space left.
    /// The caller has to ensure the data does not outlife the `Frame`
    #[inline(always)]
        pub unsafe fn place_raw<T>(&mut self, count: usize) -> *mut T {
        let size = count * mem::size_of::<T>();
        let pad = padding_for::<T>(self.top);
        let ptr = self.top + pad;
        let new_top = ptr + size;
        
        if new_top >= self.cap {
            panic!("Out of Memory");
        }
        // update top pointer
        self.top = new_top;
        ptr as *mut T
    }

    /* Placer was removed. keep code until something new is there
    /// reserve a space to place something in
    pub fn hole<T>(&mut self) -> Hole<'a, T> {
        let ptr = unsafe { self.place_raw(1) };
        Hole { ptr: ptr, _m: PhantomData }
    }
    */
    
    /// Drain the provided Iterator and return the resulting data.
    /// The iterator may not use the stack allocator.
    /// panics if the iterator attempts to use the stack allocator.
    #[inline]
    pub fn place_iter<I, T>(&mut self, mut iter: I) -> Result<Vec<'a, T>, OutOfMemory>
        where I: Iterator<Item=T>
    {
        let old_top = self.top; // in case this fails
        let pad = padding_for::<T>(old_top);
        let mut p = old_top + pad;
        let mut arr = Vec { ptr: p as *mut T, len: 0, size: 0, _m: PhantomData };
        
        // drain the iterator
        while let Some(e) = iter.next() {
            if p + mem::size_of::<T>() > self.cap {
                self.top = old_top;
                // the data will be dropped now
                #[cold]
                return Err(OutOfMemory{});
            }
            unsafe {
                ptr::write(p as *mut T, e);
            }
            p += mem::size_of::<T>();
            arr.len += 1;
        }
        arr.size = arr.len;
        self.top = p;
        
        Ok(arr)
    }
    
    /// Create a subframe.
    /// All allocations that happen within it,
    /// will be reset when it goes out if scope.
    #[inline(always)]
    pub fn subframe(&mut self) -> Frame {
        Frame {
            top:    self.top,
            cap:    self.cap,
            _m:     PhantomData
        }
    }
    
    /// Create a growing Buffer that supports io::Write
    /// call `finish()` on the buffer to complete it.
    #[cfg(feature = "std")]
    #[inline(always)]
    pub fn cursor<'b>(&'b mut self) -> Cursor<'a, 'b> {
        Cursor {
            ptr:    self.top as *mut u8,
            len:    0,
            cap:    self.cap - self.top,
            frame:  self
        }
    }
    
    /// Create a growing Buffer that supports fmt::Write
    /// call `finish()` on the buffer to complete it.
    #[inline(always)]
    pub fn writer<'b>(&'b mut self) -> Writer<'a, 'b> {
        Writer {
            ptr:    self.top as *mut u8,
            len:    0,
            cap:    self.cap - self.top,
            frame:  self
        }
    }
}

/// 'a: lifetime of the frame
/// 'b: the borrow of &mut Frame
#[cfg(feature = "std")]
pub struct Cursor<'a, 'b> where 'a: 'b  {
    ptr:    *mut u8,
    len:    usize,
    cap:    usize, // in bytes,
    frame:  &'b mut Frame<'a>
}
#[cfg(feature = "std")]
impl<'a, 'b> Cursor<'a, 'b> {
    pub fn finish(self) -> &'a mut [u8] {
        self.frame.top += self.len;
        
        unsafe {
            slice::from_raw_parts_mut(self.ptr, self.len)
        }
    }
}
#[cfg(feature = "std")]
impl<'a, 'b> io::Write for Cursor<'a, 'b> {
    #[inline(always)]
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        if self.len + buf.len() <= self.cap {
            unsafe {
                ptr::copy(
                    buf.as_ptr(),
                    self.ptr.offset(self.len as isize),
                    buf.len()
                );
            }
            self.len += buf.len();
            Ok(buf.len())
        } else {
            Err(io::ErrorKind::Other.into())
        }
    }
    fn flush(&mut self) -> io::Result<()> {
        Ok(())
    }
}

/// 'a: lifetime of the frame
/// 'b: the borrow of &mut Frame
pub struct Writer<'a, 'b> where 'a: 'b {
    ptr:    *mut u8,
    len:    usize,
    cap:    usize, // in bytes,
    frame:  &'b mut Frame<'a>
}
impl<'a, 'b> Writer<'a, 'b> {
    pub fn finish(self) -> &'a str {
        self.frame.top += self.len;
        
        unsafe {
            let slice = slice::from_raw_parts(self.ptr, self.len);
            str::from_utf8_unchecked(slice)
        }
    }
}
impl<'a, 'b> fmt::Write for Writer<'a, 'b> {
    #[inline(always)]
    fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
        if self.len + s.len() <= self.cap {
            unsafe {
                ptr::copy(s.as_ptr(), self.ptr.offset(self.len as isize), s.len());
            }
            self.len += s.len();
            Ok(())
        } else {
            Err(fmt::Error::default())
        }
    }
    #[inline(always)]
    fn write_char(&mut self, c: char) -> Result<(), fmt::Error> {
        if self.len + 4 <= self.cap {
            let slice = unsafe {
                slice::from_raw_parts_mut(
                    self.ptr.offset(self.len as isize),
                    self.cap
                )
            };
            self.len += c.encode_utf8(slice).len();
            Ok(())
        } else {
            Err(fmt::Error::default())
        }
    }
}

/// similar to the normal Vec, but stored in a Frame
pub struct Vec<'a, T: 'a> {
    ptr:    *mut T,
    size:   usize,
    len:    usize,
    _m:     PhantomData<&'a T>
}
impl<'a, T: 'a> Vec<'a, T> {
    #[inline(always)]
    unsafe fn from_raw(ptr: *mut T, size: usize) -> Vec<'a, T> {
        Vec { ptr, size, len: 0, _m: PhantomData }
    }
    /// Try appending `item` to self.
    /// Returns `None` on success and `Some(item)` if no space was remaining.
    pub fn push(&mut self, item: T) -> Option<T> {
        if self.len < self.size {
            unsafe {
                ptr::write(self.ptr.offset(self.len as isize), item);
            }
            self.len += 1;
            None
        } else {
            Some(item)
        }
    }
    
    /// Try removing one element from the end.
    /// Returns `None` if empty and `Some(item)` if one was removed.
    pub fn pop(&mut self) -> Option<T> {
        if self.len > 0 {
            self.len -= 1;
            unsafe {
                Some(ptr::read(self.ptr.offset(self.len as isize)))
            }
        } else {
            None
        }
    }
    pub fn extend(&mut self, mut iter: impl Iterator<Item=T>) {
        for idx in self.len .. self.size {
            match iter.next() {
                Some(item) => unsafe {
                    ptr::write(self.ptr.offset(idx as isize), item)
                },
                None => break
            }
            self.len += 1;
        }
    }
}
impl<'a, T> Drop for Vec<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        // drop elements
        for i in 0 .. self.len {
            unsafe { ptr::drop_in_place(self.ptr.offset(i as isize)) };
        }
    }
}
impl<'a, T> Deref for Vec<'a, T> {
    type Target = [T];
    #[inline(always)]
    fn deref(&self) -> &[T] {
        unsafe { slice::from_raw_parts(self.ptr, self.len) }
    }
}
impl<'a, T> DerefMut for Vec<'a, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut [T] {
        unsafe { slice::from_raw_parts_mut(self.ptr, self.len) }
    }
}
impl<'a, T: fmt::Debug> fmt::Debug for Vec<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        (self as &[T]).fmt(f)
    }
}
pub struct FBox<'a, T: 'a> {
    ptr:    *mut T,
    _m:     PhantomData<&'a T>
}
impl<'a, T: 'a> Drop for FBox<'a, T> {
    #[inline(always)]
    fn drop(&mut self) {
        unsafe { ptr::drop_in_place(self.ptr) }
    }
}
impl<'a, T> Deref for FBox<'a, T> {
    type Target = T;
    #[inline(always)]
    fn deref(&self) -> &T {
        unsafe { &*self.ptr }
    }
}
impl<'a, T> DerefMut for FBox<'a, T> {
    #[inline(always)]
    fn deref_mut(&mut self) -> &mut T {
        unsafe { &mut *self.ptr }
    }
}
impl<'a, T, U> PartialEq<U> for FBox<'a, T> where T: PartialEq<U> {
    fn eq(&self, rhs: &U) -> bool {
        (self as &T).eq(rhs)
    }
}
impl<'a, T: fmt::Debug> fmt::Debug for FBox<'a, T> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        (self as &T).fmt(f)
    }
}

/* placer api was removed. keep code anyway

pub struct Hole<'a, T: 'a> {
    ptr:    *mut T,
    _m:     PhantomData<&'a T>
}

impl<'a, T: 'a> Placer<T> for Hole<'a, T> {
    type Place = Hole<'a, T>;
    #[inline(always)]
    fn make_place(self) -> Self::Place {
        self
    }
}
impl<'a, T> Place<T> for Hole<'a, T> {
    #[inline(always)]
    fn pointer(&mut self) -> *mut T {
        self.ptr
    }
}
impl<'a, T> InPlace<T> for Hole<'a, T> {
    type Owner = FBox<'a, T>;
    #[inline(always)]
    unsafe fn finalize(self) -> FBox<'a, T> {
        FBox { ptr: self.ptr, _m: PhantomData }
    }
}

#[test]
fn test_place() {
    let mut data = [u8; 1024];
    let mut f = Frame::from_slice(&mut data);
    
    let a = f.hole() <- "hi";
    let b = f.hole() <- 42;
    
    assert_eq!(a, "hi");
    assert_eq!(b, 42);
}
*/
